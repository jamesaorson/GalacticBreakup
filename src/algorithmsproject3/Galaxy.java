package algorithmsproject3;

import java.util.HashMap;
import java.util.Map;

/*
 * Galaxy class provides a template for Galaxy objects
 * made up of node objects. It also makes use of a hashmap
 * to keep track of disjoint sets.
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: DisjointSet.java
 * Created: Oct 2017
 * ęCopyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

public class Galaxy {
    private final Map<Node, DisjointSet> disjointSets;
    private final Node[][][] map;
    
    public Galaxy(int x, int y, int z) {
        this.map = new Node[x][y][z];
        this.disjointSets = new HashMap<>();
    }
    
    public int getSetCount() {
        return this.disjointSets.size();
    }
    
    public void makeSet(Node node) {
        Coord coord = node.getCoordinate();
        
        this.map[coord.getX()][coord.getY()][coord.getZ()] = node;
        this.disjointSets.put(node, new DisjointSet(node));
        
        unionNeighbors(node);
    }
    
    private void unionNeighbors(Node node) {
        Coord coord = node.getCoordinate();
        
        int x = coord.getX();
        int y = coord.getY();
        int z = coord.getZ();
        Node representative = findSet(node);
        
        checkX(x, y, z, representative);
        checkY(x, y, z, representative);
        checkZ(x, y, z, representative);
    }
    
    private void checkX(int x, int y, int z, Node repA) {
        Node repB;
        
        if (x < this.map.length - 1) {
            repB = findSet(this.map[x + 1][y][z]);
            checkAndPerformUnion(repA, repB);
        }
        if (x > 0) {
            repB = findSet(this.map[x - 1][y][z]);
            checkAndPerformUnion(repA, repB);
        }
    }
    
    private void checkY(int x, int y, int z, Node repA) {
        Node repB;
        
        if (y < this.map[0].length - 1) {
            repB = findSet(this.map[x][y + 1][z]);
            checkAndPerformUnion(repA, repB);
        }
        if (y > 0) {
            repB = findSet(this.map[x][y - 1][z]);
            checkAndPerformUnion(repA, repB);
        }
    }
    
    private void checkZ(int x, int y, int z, Node repA) {
        Node repB;
        
        if (z < this.map[0][0].length - 1) {
            repB = findSet(this.map[x][y][z + 1]);
            checkAndPerformUnion(repA, repB);
        }
        if (z > 0) {
            repB = findSet(this.map[x][y][z - 1]);
            checkAndPerformUnion(repA, repB);
        }
    }
    
    private Node findSet(Node node) {
        if (node == null) {
            return null;
        }
        
        Node parent = node.getParent();
        
        if (node != parent) {
            node.setParent(findSet(parent));
        }
        
        return node.getParent();
    }
    
    //unions nodes
    private void checkAndPerformUnion(Node repA, Node repB) {
        if (repA != null && repB != null && repA != repB) {
            performUnion(this.disjointSets.get(repA),
                         this.disjointSets.get(repB));
        }
    }
    
    //unions sets
    private void performUnion(DisjointSet setA, DisjointSet setB) {
        if (setA != null && setB != null) {
            Node setAHead = setA.getHead();
            Node setBHead = setB.getHead();

            if (setAHead.getHeight() == setBHead.getHeight()) {
                setBHead.setHeight(setBHead.getHeight() + 1);

                union(setAHead, setBHead);
            } else if (setAHead.getHeight() < setBHead.getHeight()) {
                union(setBHead, setAHead);
            } else {
                union(setAHead, setBHead);
            }
        }
    }
    
    private void union(Node headA, Node headB) {
        headB.setParent(headA);
        
        if (this.disjointSets.containsKey(headB)) {
            this.disjointSets.remove(headB);
        }
    }
}