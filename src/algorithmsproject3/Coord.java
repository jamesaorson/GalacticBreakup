package algorithmsproject3;

/*
 * Coord class provides a template for 3d Coordinate objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: Coord.java
 * Created: Oct 2017
 * ęCopyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

public class Coord {
    private int x;
    private int y;
    private int z;
    
    public Coord() {
        this(-1, -1, -1);
    }
    
    public Coord(Coord coordinate) {
        this(coordinate.getX(), coordinate.getY(), coordinate.getZ());
    }
    
    public Coord(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public int getZ() {
        return this.z;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public void setZ(int z) {
        this.z = z;
    }
}
